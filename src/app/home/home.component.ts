import { Component, OnInit } from '@angular/core';
import {LoadingService} from '../core/services/loading.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  public loading$ = this.loader.loading$
  subMenuState:boolean = false;
  constructor(private loader: LoadingService) {}

  burgerClicked(event:any){
    this.subMenuState = event;
  }

  ngOnInit(): void {
  }

}
