import { Component } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import Swal from "sweetalert2";

import { UsersService } from "src/app/core/services/users.service";


@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
})

export class LoginComponent {
  public formSend: boolean = false;
  public loginForm = this.formBuilder.group({
    
    email: ["",[ Validators.required,
                Validators.email
                ]],
    password: ["", [Validators.required, Validators.minLength(8)]],
    remember: [false]
  });

  constructor(private router: Router, 
  private formBuilder: FormBuilder,
  private _userService: UsersService) {}
  
  login() {
    console.log(this.loginForm.value)
    this._userService.LoginUser(this.loginForm.value)
    .subscribe( res => { 
      console.log(res)
      Swal.fire("Hello", "Login Done", "success");
    
  },(err) => {
    Swal.fire("error", err.error.msg, "error");
  }
);




   // this.router.navigateByUrl('/')
  }
}
