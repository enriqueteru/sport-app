import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import Swal from "sweetalert2";

import { UsersService } from "src/app/core/services/users.service";
@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
})
export class RegisterComponent implements OnInit {
  public formSend: boolean = false;
  public registerForm = this.formBuilder.group(
    {
      name: ["", [Validators.required, Validators.minLength(2)]],
      subname: ["", [Validators.required, Validators.minLength(2)]],
      DNI: ["", [Validators.required, Validators.minLength(8)]],
      password: ["", [Validators.required, Validators.minLength(8)]],
      password2: ["", [Validators.required, Validators.minLength(8)]],
      phone: ["", [Validators.required]],
      email: [
        "",
        [
          Validators.required,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
        ],
      ],
      role: ["", [Validators.required]],
      country: ["", [Validators.required]],
      city: ["", [Validators.required]],
      CP: ["", [Validators.required]],
      street: ["", [Validators.required]],
      number: ["", [Validators.required]],
      moreInfo: [""],
      terms: [false, [Validators.required]],
    },
    Validators
  ); // TODO: VALIDATORS PASSWORDS

  constructor(
    private formBuilder: FormBuilder,
    private _usersService: UsersService
  ) {}

  ngOnInit(): void {}

  createUser() {
    this.formSend = true;

    if (this.registerForm.invalid) {
      return;
    }

    this._usersService.createUser(this.registerForm.value).subscribe(
      (res) => {
        console.log(res);
        Swal.fire("success", "Usuario creado", "success");
      },
      (err) => {
        Swal.fire("error", err.error.msg, "error");
      }
    );
  }

  noValidInputForm(input: string): boolean {
    if (this.registerForm.get(input)?.invalid && this.formSend) {
      return true;
    } else {
      return false;
    }
  }

  acceptTerms() {
    return !this.registerForm.get("terms")?.value && this.formSend;
  }

  equalPasswords() {
    const pass1 = this.registerForm.get("password")?.value;
    const pass2 = this.registerForm.get("password2")?.value;

    if (pass1 != pass2 && this.formSend) {
      return true;
    } else {
      return false;
    }
  }
}
