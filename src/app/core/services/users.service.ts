import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators'


import { environment } from 'src/environments/environment';


import { registerForm } from '../interfaces/registerForm.interface'
import {loginForm } from '../interfaces/loginForm.interface'


const url_API = environment.url_API;

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) {  }

  createUser (formData: registerForm){
  return this.http.post(`${url_API}users/register`, formData)
  }

  LoginUser(formData: loginForm){
    return this.http.post(`${url_API}users/authenticate`, formData)

  }
}
