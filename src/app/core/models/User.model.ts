export class User {
  constructor(
    public name: string,
    public subname: string,
    public DNI: string,
    public password: string,
    public password2: string,
    public phone: number,
    public email: string,
    public role: string,
    public country: string,
    public city: string,
    public PD: string,
    public street: string,
    public number: number,
    public moreInfo: string
      
    
  ) {}
}
