export interface registerForm {
  name: string;
  subname: string;
  DNI: string;
  password: string;
  phone: number;
  email: string;
  role: string;
  country: string;
  city: string;
  CP: string;
  street: string;
  number: number;
  moreInfo: string;
}
