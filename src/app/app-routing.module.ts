import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthRoutingModule } from './auth/routes/auth.routing';

import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './core/page-not-found/page-not-found.component';
import { ProfileRoutingModule } from './profile/routes/profile.routes';




const routes: Routes = [
  //Profile
  //auth ---> /register /Login
  { path: '', component: HomeComponent, data: {
    title: "Home"
  } },
  { path: '**', component: PageNotFoundComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes), AuthRoutingModule, ProfileRoutingModule],
  exports: [RouterModule]
})
export class AppRoutingModule {}
