import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { CoreModule } from '../core/core.module';


import { BreadcrumbsComponent } from './layout/breadcrumbs/breadcrumbs.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import { TabBarComponent } from './layout/tab-bar/tab-bar.component';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './layout/sidebar/sidebar.component';




@NgModule({
  declarations: [
    BreadcrumbsComponent,
    HeaderComponent,
    FooterComponent,
    TabBarComponent,
    SidebarComponent,

  ],
  imports: [
    CommonModule,
    CoreModule,
    RouterModule
  ],

  exports: [
    BreadcrumbsComponent,
    HeaderComponent,
    FooterComponent,
    TabBarComponent,
    SidebarComponent




  ],
})
export class SharedModule { }
